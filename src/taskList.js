import React from "react";
import { View,Button, TouchableOpacity, Text } from "react-native";
import { FlatList } from "react-native-gesture-handler";

const TaskList = ({ navigation }) => {
    const tasks = [
        {
            id: 1,
            title: 'comer',
            date: '2028-07-31',
            time: '23:59',
            address: 'Comida',
        },
        {
            id: 2,
            title: 'estudar',
            date: '2024-03-01',
            time: '18:00',
            address: 'Casa',
        },
        {
            id: 3,
            title: 'trabalhar',
            date: '2024-03-02',
            time: '09:00',
            address: 'Escritório',
        },
    ];
    const taskPress=(task)=>{
        navigation.navigate('DetalhesDasTarefas',{task})   
    }
    return(
        <View>
            <FlatList
            data={tasks}
            keyExtractor={(item)=> item.id.toString}
            renderItem={({item}) => (
                <TouchableOpacity onPress={() =>taskPress(item)}>
                    <Text>{item.title}</Text>
                </TouchableOpacity>
            )}
            />
        </View>
)
}
export default TaskList;