import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import TaskList from './src/taskList';
import TaskDetail from './src/taskDetail';

const  stack = createStackNavigator();


export default function App() {
  return (
    <NavigationContainer>
      <stack.Navigator initialRouteName=''>
        <stack.Screen  name="ListaDeTarefas" component={TaskList}/>
        <stack.Screen  name="DetalhesDasTarefas" component={TaskDetail}
        options={{title:"Informações das Tarefas"}}
        />
      </stack.Navigator>
    </NavigationContainer>
  );
};
